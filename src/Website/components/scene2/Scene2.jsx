import {
  Environment,
  OrbitControls,
  PerspectiveCamera,
} from '@react-three/drei'
import Model from './Model'
import { Suspense } from 'react'
import Placeholder from './Placeholder'
import { useFrame } from '@react-three/fiber'

export default function Scene2() {
  useFrame(({ clock, camera }) => {
    const elapsedTime = clock.getElapsedTime()

    // Calculate new camera position
    const radius = 17
    const xPos = radius * Math.cos(elapsedTime / 6)
    const zPos = radius * Math.sin(elapsedTime / 6)
    const yPos = 4

    // Offset the camera position based on the model's position
    const modelPosition = [2, -1, 0] // Example model position
    camera.position.set(
      xPos + modelPosition[0],
      yPos + modelPosition[1],
      zPos + modelPosition[2]
    )

    // Rotate the camera to point towards the model
    camera.lookAt(modelPosition[0], modelPosition[1], modelPosition[2])
  })

  return (
    <>
      {/* <OrbitControls makeDefault /> */}
      {/* <PerspectiveCamera /> */}
      <Environment
        files={'./envMap/street3.hdr'}
        background
        // preset='sunset'
        // ground={{
        //   height: 9,
        //   radius: 33,
        //   scale: 100,
        // }}
      />
      <directionalLight
        castShadow
        position={[1, 2, 3]}
        intensity={22}
      />
      <ambientLight intensity={22} />
      <Suspense fallback={<Placeholder />}>
        <Model />
      </Suspense>
    </>
  )
}
