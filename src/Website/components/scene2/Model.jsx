import React from 'react'
import { useLoader } from '@react-three/fiber'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import { useGLTF } from '@react-three/drei'

export default function Model() {
  //   const model = useLoader(GLTFLoader, './porsche/scene.gltf', (loader) => {
  //     const dracoLoader = new DRACOLoader()
  //     dracoLoader.setDecoderPath('./draco/')
  //     loader.setDRACOLoader(dracoLoader)
  //   })
  const model = useGLTF('./porsche/scene.gltf')
  return (
    <primitive
      object={model.scene}
      position-x={2}
      position-y={-1}
      scale={3.8}
    />
  )
}
