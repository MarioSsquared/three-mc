import {
    Environment, MeshReflectorMaterial,
    OrbitControls,
    PerspectiveCamera,
    Sky,
} from '@react-three/drei'
import Model from './Model'
import React, { Suspense } from 'react'
import Placeholder from './Placeholder'
import { useFrame } from '@react-three/fiber'
import { TextureLoader } from 'three'
import * as THREE from 'three'
import DarkSky from './DarkSky'

export default function Scene3() {
  const textureLoader = new TextureLoader()
  const woodTexture = textureLoader.load('./textures/wood_floor_diff_4k.jpg') // Adjust the path to your diffuse texture
  const normalMap = textureLoader.load('./textures/wood_floor_nor_gl_4k.jpg') // Adjust the path to your normal map texture
  const roughnessMap = textureLoader.load('./textures/wood_floor_rough_4k.jpg') // Adjust the path to your roughness map texture
  const aoMap = textureLoader.load('./textures/wood_floor_ao_4k.jpg') // Adjust the path to your AO map texture
  const material = new THREE.MeshStandardMaterial({
    map: woodTexture, // Diffuse texture
    normalMap: normalMap, // Normal map
    roughnessMap: roughnessMap, // Roughness map
    aoMap: aoMap, // Ambient occlusion map
    metalness: 0.8,
    roughness: 0.2,
  })
  useFrame(({ clock, camera }) => {
    const elapsedTime = clock.getElapsedTime()

    // Calculate new camera position
    const radius = 17
    const xPos = radius * Math.cos(elapsedTime / 6)
    const zPos = radius * Math.sin(elapsedTime / 6)
    const yPos = 4

    // Offset the camera position based on the model's position
    const modelPosition = [2, -1, 0] // Example model position
    camera.position.set(
      xPos + modelPosition[0],
      yPos + modelPosition[1],
      zPos + modelPosition[2]
    )

    // Rotate the camera to point towards the model
    camera.lookAt(modelPosition[0], modelPosition[1], modelPosition[2])
  })

  return (
    <>
      {/* <OrbitControls makeDefault /> */}
      {/* <PerspectiveCamera /> */}
      {/* <Environment
        // files={'./envMap/street3.hdr'}
        background
        preset='sunset'
        // ground={{
        //   height: 9,
        //   radius: 33,
        //   scale: 100,
        // }}
      /> */}
        <color attach="background" args={["#213547"]}/>
        <fog attach="fog" args={['#213547', 12, 28]}/>
      <directionalLight
        castShadow
        position={[1, 11, 14]}
        intensity={22}
      />

      {/*<DarkSky />*/}
      {/* <mesh
        position-y={-3}
        position-x={3}
        rotation-x={-Math.PI * 0.5}
        scale={22}
      >
        <planeGeometry />
        <meshStandardMaterial color='red' />
      </mesh> */}
      {/*<mesh*/}
      {/*  position-y={-1.5}*/}
      {/*  position-x={1.8}*/}
      {/*  position-z={1}*/}
      {/*  scale={[9, 1, 11]}*/}
      {/*  material={material}*/}
      {/*>*/}
      {/*  <cylinderGeometry args={[2, 2, 0.5, 110, 110]} />*/}
      {/*</mesh>*/}
        <mesh rotation={[-Math.PI / 2, 0, 0]} position-y={-2}>
            <planeGeometry args={[170, 170]} />
            <MeshReflectorMaterial
                blur={[300, 100]}
                resolution={2048}
                mixBlur={1}
                mixStrength={40}
                roughness={1}
                depthScale={1.2}
                minDepthThreshold={0.4}
                maxDepthThreshold={1.4}
                color="#101010"
                metalness={0.5}
            />
        </mesh>
      <ambientLight intensity={2} />
      <Suspense fallback={<Placeholder />}>
        <Model />
      </Suspense>
    </>
  )
}
