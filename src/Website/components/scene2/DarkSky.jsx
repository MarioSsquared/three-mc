import { useThree } from '@react-three/fiber'
import * as THREE from 'three'

export default function DarkSky() {
  const { scene } = useThree()

  // Define gradient colors
  const gradientTop = new THREE.Color('#121b52') // Darker color at the top
  const gradientBottom = new THREE.Color('#979cc7') // Lighter color at the bottom

  // Create a custom shader for gradient background
  const gradientShader = {
    uniforms: {
      topColor: { value: gradientTop },
      bottomColor: { value: gradientBottom },
      offset: { value: 400 },
      exponent: { value: 0.6 },
    },
    vertexShader: `
      varying vec3 vWorldPosition;
      void main() {
        vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
        vWorldPosition = worldPosition.xyz;
        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
      }
    `,
    fragmentShader: `
      uniform vec3 bottomColor;
      uniform vec3 topColor;
      uniform float offset;
      uniform float exponent;
      varying vec3 vWorldPosition;
      void main() {
        float h = normalize( vWorldPosition + offset ).y;
        gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( max( h , 0.0 ), exponent ), 0.0 ) ), 1.0 );
      }
    `,
  }

  // Create a shader material using the gradient shader
  const gradientMaterial = new THREE.ShaderMaterial({
    uniforms: gradientShader.uniforms,
    vertexShader: gradientShader.vertexShader,
    fragmentShader: gradientShader.fragmentShader,
    side: THREE.BackSide, // Render on the inside of the sphere
  })

  // Create a sphere geometry to serve as the background
  const sphereGeometry = new THREE.SphereGeometry(500, 32, 32)
  const gradientSphere = new THREE.Mesh(sphereGeometry, gradientMaterial)

  // Add the gradient sphere to the scene
  scene.add(gradientSphere)

  return null // This component doesn't render anything, it only adjusts the scene background
}
