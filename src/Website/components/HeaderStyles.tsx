import {createStyles, makeStyles} from "@mui/styles";


export default makeStyles(() =>
  createStyles({
    container: {
      borderBottom: '1px solid #CCCCCC',
      position: 'sticky',
      top: 0,
      zIndex: 140,
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      height: '64px',
      maxHeight: '64px',
      background: 'white',
      transition: 'ease-in-out 0.3s',
      // maxWidth: '1440px',
      margin: '0 auto',
    },
    navLink: {
      padding: '6px 18px',
      textDecoration: 'none',
      color: '#060707',
      fontSize: '13px',
      fontFamily: 'Nunito Sans',
      fontWeight: 400,
      lineHeight: '19px',
      opacity: '0.7',
      textTransform: 'uppercase',
    },
    activeNavLink: {
      padding: '6px 18px',
      textDecoration: 'none',
      color: '#000000',
      fontSize: '13px',
      fontFamily: 'Nunito Sans',
      fontWeight: 800,
      lineHeight: '19px',
      textTransform: 'uppercase',
    },
  })
)
