import React, { useState } from 'react'
import styles from './HeaderStyles'
import AvoNavBarLogo from '../assets/AvoLogo.png'
import { Box, Divider, Typography } from '@mui/material'

export function Header() {
  const classes = styles()
  const [open, setOpen] = useState(false)

  return (
    <Box className={classes.container}>
      <Box
        display='flex'
        justifyContent='space-between'
        width='100%'
        pt={1}
        pb={1}
      >
        <Box
          display='flex'
          pl='50px'
          alignItems='center'
        >
          <Box>
            <img
              style={{ cursor: 'pointer' }}
              width={78}
              src={AvoNavBarLogo}
              alt='Logo'
            />
          </Box>
          <Divider
            orientation='vertical'
            style={{
              height: '35px',
              marginRight: '20px',
              marginLeft: '8.25px',
            }}
          />
          <Typography
            sx={{
              size: '18px',
              fontFamily: 'sans-serif',
              lineHeight: '24px',
              color: '#000000',
              fontWeight: 600,
              cursor: 'pointer',
            }}
          >
            Auto
          </Typography>
        </Box>
        <Box
          width='100%'
          display='flex'
          justifyContent='center'
          alignItems='center'
        >
          <Typography className={classes.navLink}>Car Listings</Typography>
          <Typography className={classes.navLink}>
            Dealership Listings
          </Typography>
          <Typography className={classes.navLink}>Tips & Reviews</Typography>
          <Typography className={classes.navLink}>
            Affordability Calculator
          </Typography>
        </Box>
      </Box>
    </Box>
  )
}
