import { useFrame } from '@react-three/fiber'
import React, { useEffect, useRef, useState } from 'react'
// import './scene.css'

export default function Scene() {
  const cubeRef = useRef()
  const sphereRef = useRef()
  const [time, setTime] = useState(0)
  //   let time = 0
  //   useEffect(() => {
  //     let animationFrameId
  //     const updateTime = (timestamp) => {
  //       time += timestamp
  //       animationFrameId = requestAnimationFrame(updateTime)
  //     }
  //     animationFrameId = requestAnimationFrame(updateTime)
  //     return () => cancelAnimationFrame(animationFrameId)
  //   }, [])

  useFrame((state, delta) => {
    setTime((time) => time + delta)
  })

  const animationSpeed = 5 // Adjust the speed of the animation
  const amplitude = 0.3 // Adjust the amplitude of the animation
  const yPos = Math.cos(time * animationSpeed) * amplitude + 0.25

  //   delta= time between frames (0.01 seconds)
  useFrame((state, delta) => {
    cubeRef.current.rotation.y += delta
    sphereRef.current.position.y = yPos
  })

  return (
    <>
      <mesh
        position={[-2, 0, 0]}
        ref={sphereRef}
      >
        {/* radius, width segments, length segments */}
        <sphereGeometry args={[1, 32, 32]} />
        <meshNormalMaterial />
      </mesh>
      <mesh
        ref={cubeRef}
        position={[2, 0, 0]}
        scale={1}
        // rotation-y={Math.PI * 0.5}
      >
        <boxGeometry />
        <meshBasicMaterial color={'orange'} />
      </mesh>
      <mesh
        position-y={-1}
        rotation-x={-Math.PI * 0.5}
        scale={10}
      >
        <planeGeometry />
        <meshNormalMaterial />
      </mesh>
    </>
  )
}
