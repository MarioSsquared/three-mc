import { Box, Paper, Typography } from '@mui/material'
import React from 'react'
import BannerImg from '../assets/home-banner.jpg'

export default function Banner() {
  return (
    <Box
      display='flex'
      justifyContent='center'
      flexDirection={'column'}
      p={4}
      style={{
        backgroundImage: `url(${BannerImg})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        minHeight: '520px',
      }}
    >
      <Box
        maxWidth='1440px'
        display={'flex'}
        justifyContent={'flex-start'}
        ml={{ xs: 0, sm: 3, lg: 20, xl: '230px' }}
      >
        <Paper
          style={{
            background: 'rgb(244 244 244)',
            maxWidth: '500px',
            borderRadius: '8px',
          }}
        >
          <Box p={4}>
            <Typography
              variant={'h5'}
              component={'h1'}
            >
              Search for new and used cars
            </Typography>
            {/* <SearchSection /> */}
          </Box>
        </Paper>
      </Box>
    </Box>
  )
}
