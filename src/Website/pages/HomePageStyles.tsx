import { createStyles, makeStyles } from '@mui/styles'

export default makeStyles(() =>
  createStyles({
    HeadingText: {
      fontFamily: 'Nunito Sans',
      fontSize: '55px',
      lineHeight: '70px',
      fontWeight: 600,
      color: '#FFFFFF',
    },
    BodyText: {
      fontFamily: 'Nunito Sans',
      fontSize: '20px',
      lineHeight: '27px',
      fontWeight: 500,
      color: '#FFFFFF',
    },
    searchBar: {
      borderRadius: '17.5px',
      maxHeight: '40px',
      '& .MuiInputLabel-outlined': {
        transform: 'translate(15px, 13px) scale(1)',
      },
      '& .MuiInputLabel-shrink': {
        transform: 'translate(15px, -6px) scale(0.75)',
      },
    },
    searchBarText: {
      fontWeight: 500,
      fontFamily: 'Nunito Sans',
      fontSize: '12px',
      lineHeight: '16px',
      color: '#666666',
    },
    Radio: {
      height: '17px',
    },
    RadioButtonText: {
      fontWeight: 500,
      fontFamily: 'Nunito Sans',
      fontSize: '12px',
      lineHeight: '16px',
      color: '#000000',
    },
    CheckedRadioButtonText: {
      fontWeight: 600,
      fontFamily: 'Nunito Sans',
      fontSize: '12px',
      lineHeight: '16px',
      color: '#000000',
    },
    SearchHeaderText: {
      fontWeight: 500,
      fontFamily: 'Nunito Sans',
      fontSize: '20px',
      lineHeight: '27px',
      color: '#000000',
      textTransform: 'uppercase',
    },
    ButtonText: {
      fontWeight: 500,
      fontFamily: 'Nunito Sans',
      fontSize: '14px',
      lineHeight: '20px',
      color: '#FFFFFF',
      textTransform: 'uppercase',
    },
    CardHeader: {
      fontWeight: 500,
      fontFamily: 'Nunito Sans',
      fontSize: '18px',
      lineHeight: '24px',
      color: '#000000',
      textTransform: 'uppercase',
    },
    MobileCardHeader: {
      fontWeight: 600,
      fontFamily: 'Nunito Sans',
      fontSize: '14px',
      lineHeight: '20px',
      color: '#000000',
      textTransform: 'uppercase',
    },
    ViewAllButtonText: {
      fontWeight: 600,
      fontFamily: 'Nunito Sans',
      fontSize: '14px',
      lineHeight: '19px',
      color: '#0059ff',
    },
    MobileViewAllButtonText: {
      fontWeight: 600,
      fontFamily: 'Nunito Sans',
      fontSize: '12px',
      lineHeight: '16px',
      color: '#0059ff',
    },
  })
)
