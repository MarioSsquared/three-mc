import React, { useContext, useEffect } from 'react'
import { Box, Button, Grid, Paper, Typography } from '@mui/material'
import Styles from './HomePageStyles'
import { Canvas } from '@react-three/fiber'

import { Header } from '../components/Header'
import Banner from '../components/Banner'
import Scene from '../components/Scene'
import Scene2 from '../components/scene2/Scene2'
import Scene3 from '../components/scene2/Scene3'

export function HomePage() {
  const classes = Styles()

  return (
    <>
      <Header />
      <Box
        display='flex'
        justifyContent='center'
        flexDirection={'column'}
        width='100%'
      >
        <Box height={'75vh'}>
          <Canvas>
            {/*<Scene2 />*/}
             <Scene3 />
          </Canvas>
        </Box>
        {/* <Banner /> */}

        <Box
          display={'flex'}
          flexDirection={'column'}
          width={'100%'}
          alignItems={'center'}
        >
          <Box
            display='flex'
            flexDirection='column'
            width={'calc(100% - 40px)'}
            maxWidth='1440px'
            mt={'20px'}
            pl={'20px'}
            pr={'20px'}
          >
            <Box
              alignItems='center'
              pl={'0px'}
              pr={'0px'}
              pt='32px'
            >
              <Box
                display='flex'
                justifyContent='space-between'
                alignItems={'center'}
              >
                <Typography className={classes.CardHeader}>
                  Tips and Reviews
                </Typography>
                {/* <Link to={'/articles'}>
                  <Button variant='text'>
                    <Typography className={classes.ViewAllButtonText}>
                      View All
                    </Typography>
                  </Button>
                </Link> */}
              </Box>
              {/* <TipsAndReviewsSection label='' /> */}
            </Box>
            <Box
              pl={'0px'}
              pt={'60px'}
            >
              <Typography className={classes.CardHeader}>Promotions</Typography>
              {/* <PromotionsSection /> */}
            </Box>
            <Box
              pl={'0px'}
              pt={'60px'}
            >
              <Typography
                className={classes.CardHeader}
                component={'h1'}
              >
                Featured Listings
              </Typography>
            </Box>

            <Box
              pt='40px'
              display='flex'
              flexDirection={'column'}
              width={'100%'}
              alignItems='center'
              pl={'0px'}
              pr={'0px'}
            >
              <Box
                display='flex'
                justifyContent='space-between'
                alignItems='center'
                width={'100%'}
                mb={2}
              >
                <Typography>Dealerships on AVO</Typography>
                <Box pl='10px'>
                  <Button variant='text'>
                    <Typography>View all</Typography>
                  </Button>
                </Box>
              </Box>
            </Box>
            <Box
              pt='40px'
              display='flex'
              flexDirection={'column'}
              width={'100%'}
              alignItems='flex-start'
              pl={'0px'}
              pr={'0px'}
            >
              <Typography className={classes.CardHeader}>About AVO</Typography>
              <Grid
                container
                style={{ marginTop: '8px' }}
                spacing={2}
              >
                <Grid
                  item
                  xs={12}
                  md={6}
                >
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      Buying a second-hand car can be difficult – there are just
                      so many options and uncertainties. Let Avo Auto, the
                      game-changing auto platform, solve your car-buying
                      problems. Shop for thousands of cars, see how much you can
                      afford, and apply for finance – all in one place.
                    </Typography>
                  </Box>
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        Transparency is our guiding light.{' '}
                      </span>
                      Transparency is more than a buzzword at Avo Auto – it’s
                      our foundation. We ensure that our car descriptions are
                      accurate and easy to understand so that you can make
                      informed decisions.
                    </Typography>
                  </Box>
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        Expect detailed insights.{' '}
                      </span>
                      Each Avo Auto listing includes in-depth details about the
                      car – from mileage to colour. And with these specifics,
                      you can truly understand what you’re buying.
                    </Typography>
                  </Box>
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        Reliable dealers mean reliable cars.{' '}
                      </span>
                      We work with trustworthy, MFC-approved dealers only to
                      ensure that the cars on our site are from reliable
                      sources. Your peace of mind is our priority!
                    </Typography>
                  </Box>
                </Grid>
                <Grid
                  item
                  xs={12}
                  md={6}
                >
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        Check your budget and apply for finance easily.{' '}
                      </span>
                      We help you calculate how much you can afford to pay for a
                      car based on your budget, with real-time responses from
                      credit bureaus. We also process car finance applications
                      to make your experience quick and easy.
                    </Typography>
                  </Box>
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        A strong connection with Nedbank.{' '}
                      </span>
                      As part of the Nedbank family, Avo Auto is perfectly
                      positioned to provide the best financial solutions,
                      including personalised options to meet your needs.
                    </Typography>
                  </Box>
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        Welcome to the future of car shopping.{' '}
                      </span>
                      Transparency, quality cars, detailed information, friendly
                      support, and financial solutions are just a few reasons
                      why you should be buying your next car through us. Throw
                      in a streamlined online journey and even more value along
                      the way, and you have yourself a great deal on a car –
                      before you even start!
                    </Typography>
                  </Box>
                  <Box mb={1}>
                    <Typography
                      variant={'body1'}
                      style={{ color: '#000' }}
                    >
                      <span style={{ fontWeight: 600 }}>
                        Let’s find the car of your dreams today. It’s just a
                        click away …
                      </span>
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  )
}
