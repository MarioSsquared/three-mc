import { OrbitControls } from '@react-three/drei'
import { useFrame, extend, useThree } from '@react-three/fiber'
import React, { Suspense, useRef, useState } from 'react'
import Model from './Model'
import Placeholder from './Placeholder'
import { TextureLoader } from 'three'

extend({ OrbitControls })

export default function Step4() {
  const cubeRef = useRef()
  const sphereRef = useRef()
  const groupRef = useRef()


  return (
    <>
      <OrbitControls />
      <directionalLight
        castShadow
        position={[1, 2, 3]}
        intensity={1}
      />
      <ambientLight intensity={5} />
      <mesh
        receiveShadow
        position-y={-1}
        rotation-x={-Math.PI * 0.5}
        scale={10}
      >
        <planeGeometry />
        <meshStandardMaterial color={'#6699CC'} />
      </mesh>
    </>
  )
}
