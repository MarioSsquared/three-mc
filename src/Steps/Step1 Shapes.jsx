import {Float, OrbitControls, Text} from '@react-three/drei'
import { useFrame, extend, useThree } from '@react-three/fiber'
import React, { useRef, useState } from 'react'

extend({ OrbitControls })

export default function Step1() {
  return (
    <>
      <group>
        <Float speed={4} floatingRange={[0,0.6]} floatIntensity={3} >
            <Text color={'red'} maxWidth={2} textAlign={'center'}>Hello Masterclass</Text>
        </Float>
      </group>
    </>
  )
}
