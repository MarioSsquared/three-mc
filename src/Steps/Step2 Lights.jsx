import { OrbitControls } from '@react-three/drei'
import { useFrame, extend, useThree } from '@react-three/fiber'
import React, { useRef, useState } from 'react'

extend({ OrbitControls })

export default function Step2() {
  return (
    <>
      <directionalLight position={[1, 2, 3]} />
      <group>
        <mesh position={[-2, 0, 0]}>
          {/* radius, width segments, length segments */}
          <sphereGeometry args={[1, 32, 32]} />
          <meshNormalMaterial />
        </mesh>
        <mesh
          position={[2, 0, 0]}
          scale={1}
        >
          <boxGeometry />
          <meshStandardMaterial color={'orange'} />
        </mesh>
        <mesh
          position-y={-1}
          rotation-x={-Math.PI * 0.5}
          scale={10}
        >
          <planeGeometry />
          <meshStandardMaterial />
        </mesh>
      </group>
    </>
  )
}
