import { Canvas } from '@react-three/fiber'
import './style.css'
import ReactDOM from 'react-dom/client'
import { HomePage } from './Website/pages/HomePage'
import Scene from './Scene'
import Scene2 from './Website/components/scene2/Scene2'
import Experience from './HM/components/Experience'
import Configurator from './HM/components/Configurator'
import { CustomizationProvider } from './HM/contexts/Customization'

const root = ReactDOM.createRoot(document.querySelector('#root'))

root.render(
  <Canvas shadows>
      {/*<color args={["#79a6fa"]} attach={"background"}/>*/}
    <Scene />
  </Canvas>
  // <>
  //   <HomePage />
  // </>
  // <CustomizationProvider>
  //   <div className='App'>
  //     <Canvas dpr={[1, 2]}>
  //       <color
  //         attach='background'
  //         args={['#213547']}
  //       />
  //       <fog
  //         attach='fog'
  //         args={['#213547', 10, 20]}
  //       />
  //       <Experience />
  //     </Canvas>
  //     <Configurator />
  //   </div>
  // </CustomizationProvider>
)
