import {OrbitControls, Sky, SoftShadows} from '@react-three/drei'
import { useFrame, extend, useThree } from '@react-three/fiber'
import React, { useRef, useState } from 'react'

extend({ OrbitControls })

export default function Step3() {
  const cubeRef = useRef()
  const sphereRef = useRef()
  const groupRef = useRef()
  const [time, setTime] = useState(0)

  useFrame((state, delta) => {
    setTime((time) => time + delta)
  })
  const directionalLight = useRef()
  const animationSpeed = 5 // Adjust the speed of the animation
  const amplitude = 0.3 // Adjust the amplitude of the animation
  const yPos = Math.cos(time * animationSpeed) * amplitude + 0.25

  //   delta= time between frames (0.01 seconds)
  useFrame((state, delta) => {
    cubeRef.current.rotation.y += delta
    // groupRef.current.rotation.y += 0.001
    sphereRef.current.position.y = yPos
  })

  return (
    <>
      {/* Needs camera, dom element (Canvas in this case) */}
      <OrbitControls />
      <SoftShadows />
      <Sky />
      <directionalLight position={[1, 2, 3]} castShadow ref={directionalLight}
                        shadow-mapSize={[2048,2048]}
      />
      <ambientLight intensity={0.5} />
      <group ref={groupRef}>
        <mesh
          position={[-2, 0, 0]}
          ref={sphereRef}
          castShadow
        >
          {/* radius, width segments, length segments */}
          <sphereGeometry args={[1, 32, 32]} />
          <meshNormalMaterial />
        </mesh>
        <mesh
          ref={cubeRef}
          position={[2, 0, 0]}
          scale={1}
          castShadow
        >
          <boxGeometry />
          <meshStandardMaterial color={'orange'} />
        </mesh>
        <mesh
          position-y={-1}
          rotation-x={-Math.PI * 0.5}
          scale={10}
          receiveShadow
        >
          <planeGeometry />
          <meshStandardMaterial />
        </mesh>
      </group>
    </>
  )
}
