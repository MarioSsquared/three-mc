export default function Placeholder() {
  return (
    <mesh
      position-y={0.5}
      scale={[2, 2, 2]}
    >
      {/*width, height, depth, widthSegments, heightSegments, depthSegments*/}
      <boxGeometry args={[1, 1, 1, 2, 2, 2]} />
      <meshBasicMaterial
        wireframe
        color='red'
      />
    </mesh>
  )
}
