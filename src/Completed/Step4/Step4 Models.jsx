import {Environment, OrbitControls} from '@react-three/drei'
import { useFrame, extend, useThree } from '@react-three/fiber'
import React, { Suspense, useRef, useState } from 'react'
import Model from './Model'
import Placeholder from './Placeholder'
import * as THREE from "three";
import { TextureLoader } from 'three'

extend({ OrbitControls })

export default function Step4() {
    const cubeRef = useRef()
    const sphereRef = useRef()
    const groupRef = useRef()

    const textureLoader = new TextureLoader()
    const woodTexture = textureLoader.load('./textures/wood_floor_diff_4k.jpg') // Adjust the path to your diffuse texture
    const normalMap = textureLoader.load('./textures/wood_floor_nor_gl_4k.jpg') // Adjust the path to your normal map texture
    const roughnessMap = textureLoader.load('./textures/wood_floor_rough_4k.jpg') // Adjust the path to your roughness map texture
    const aoMap = textureLoader.load('./textures/wood_floor_ao_4k.jpg') // Adjust the path to your AO map texture

    // Repeat
    const repeatFactor = 3;
    woodTexture.repeat.set(repeatFactor, repeatFactor)
    normalMap.repeat.set(repeatFactor, repeatFactor)
    roughnessMap.repeat.set(repeatFactor, repeatFactor)
    aoMap.repeat.set(repeatFactor, repeatFactor)

    // Ensure the texture repeats
    woodTexture.wrapS = woodTexture.wrapT = THREE.RepeatWrapping
    normalMap.wrapS = normalMap.wrapT = THREE.RepeatWrapping
    roughnessMap.wrapS = roughnessMap.wrapT = THREE.RepeatWrapping
    aoMap.wrapS = aoMap.wrapT = THREE.RepeatWrapping

    const material = new THREE.MeshStandardMaterial({
        map: woodTexture, // Diffuse texture
        normalMap: normalMap, // Normal map
        roughnessMap: roughnessMap, // Roughness map
        aoMap: aoMap, // Ambient occlusion map
        metalness: 0.1,
        roughness: 0.8,
    })

    return (
        <>

                <OrbitControls />
                {/*<directionalLight*/}
                {/*    castShadow*/}
                {/*    position={[1, 2, 3]}*/}
                {/*    intensity={1}*/}
                {/*/>*/}
                {/*<ambientLight intensity={2} />*/}
            <Environment preset={'forest'} resolution={128} background />
                <mesh
                    receiveShadow
                    position-y={-1}
                    rotation-x={-Math.PI * 0.5}
                    scale={10}
                    material={material}
                >
                    <planeGeometry />
                </mesh>
                <Suspense fallback={<Placeholder />}>
                    <Model />
                </Suspense>
        </>
    )
}
