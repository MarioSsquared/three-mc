import React from 'react'
import { useGLTF } from '@react-three/drei'

export default function Model() {
  const model = useGLTF('./buster_drone/scene.gltf')
  return (
    // basic object to render fundamental models
    <primitive
      object={model.scene}
      scale={1}
      position-y={0.3}
    />
  )
}
