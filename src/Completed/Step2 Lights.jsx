import {BakeShadows, OrbitControls, Sky, SoftShadows, useHelper} from '@react-three/drei'
import { useFrame, extend, useThree } from '@react-three/fiber'
import React, { useRef, useState } from 'react'
import * as THREE from 'three'

extend({ OrbitControls })

export default function Step2() {
    const directionalLight = useRef()
    useHelper(directionalLight, THREE.DirectionalLightHelper, 1)
  return (
    <>
      {/*<BakeShadows/>*/}
        <SoftShadows />
      <OrbitControls />
        <Sky/>
      <directionalLight position={[1, 2, 3]} castShadow ref={directionalLight}
        shadow-mapSize={[2048,2048]}
      />
      <ambientLight intensity={0.5} />
      <group>
        <mesh position={[-2, 0, 0]} castShadow>
          {/* radius, width segments, length segments */}
          <sphereGeometry args={[1, 32, 32]} />
          <meshNormalMaterial />
        </mesh>
        <mesh
          position={[2, 0, 0]}
          scale={1}
          castShadow
        >
          <boxGeometry />
          <meshStandardMaterial color={'orange'} />
        </mesh>
        <mesh
          position-y={-1}
          rotation-x={-Math.PI * 0.5}
          scale={10}
          receiveShadow
        >
          <planeGeometry />
          <meshStandardMaterial />
        </mesh>
      </group>
    </>
  )
}
